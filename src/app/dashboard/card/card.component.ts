import { Component, OnInit, Input } from '@angular/core';
import { ProbeResultGroup } from '../../model/probe-result';
import { ActivatedRoute, Router } from '@angular/router';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';
import { FilterType } from 'src/app/label/label';

@Component({
  selector: 'sw-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  private _groupKey: string = null

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private filterContext: FilteringContextService,
  ) { }

  @Input() 
  group: ProbeResultGroup = null
  
  @Input() 
  set groupKey(groupKey: string){
    console.log(groupKey)
    this._groupKey = groupKey;
  }
  

  ngOnInit() {
  }

  onSelectHome(host: string){

    this.filterContext.addFilter({
      "key": this._groupKey,
      "value": this.group.name,
      "type": FilterType.LABEL,
     });

    this.router.navigate(["hosts", host, "detail"]);
  }

}
