import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProbeResultService } from '../../probe-results/probe-result.service';
import { Subscription, timer, Observable } from 'rxjs';
import { ProbeLastResultCriteria, ProbeResult } from 'src/app/model/probe-result';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';

@Component({
  selector: 'app-host-detail',
  templateUrl: './host-detail.component.html',
  styleUrls: ['./host-detail.component.scss']
})
export class HostDetailComponent implements OnInit, OnDestroy {

  public host: string
  private timerSubscription: Subscription = null
  private reloadInterval  = 5000;
  public results$: Observable<ProbeResult[]> = null;
  
  ctxtChangeSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private probeResultService: ProbeResultService,
    private filterContext: FilteringContextService,
  ) { 

    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        this.refreshData()
    });

    this.route.params.subscribe(params => {
      this.host = params["name"];
      this.refreshData()
    }); 
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.ctxtChangeSubscription.unsubscribe();
  }
  private subscribeToData(): void {

    this.timerSubscription = timer(0, this.reloadInterval).subscribe(() => this.refreshData())

  }

  refreshData(){
    var criteria: ProbeLastResultCriteria = {
      host: this.host,
      query: this.filterContext.getQuery(),
      labels: this.filterContext.serialiseLabels()
    }

    this.results$ = this.probeResultService.getLastResults(criteria)
  }

  onSelectResult(probeName: string){
    this.router.navigate(["probes", probeName, "detail"]);
  }



}
