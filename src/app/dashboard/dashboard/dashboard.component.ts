import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProbeResultService } from '../../probe-results/probe-result.service';
import { timer, Subscription, Observable } from 'rxjs';
import { ProbeResultGroup, GroupStatusProbeCriteria } from '../../model/probe-result';
import { LabelService } from 'src/app/label/label.service';
import { Label, KVFilter, FilterType } from 'src/app/label/label';
import { LabelSelectorDialog } from 'src/app/label/dialog/label-selector/label-selector.dialog';
import { MatDialog } from '@angular/material';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';
import { allowPreviousPlayerStylesMerge } from '@angular/animations/browser/src/util';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {


  private timerSubscription: Subscription = null
    
  ctxtChangeSubscription: Subscription;

  public group$: Observable<ProbeResultGroup> = null;
  
  // Dashboard reload interval in milliseconds
  private reloadInterval = 30000;

  public labels : Label[] = [];
  
  constructor(
    private router: Router,
    private labelService: LabelService,
    private probeResultService: ProbeResultService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private filterContext: FilteringContextService,
  ) { 

    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        this.refreshData()
    });
  }

  get label(): string{
    return this.filterContext.getGroup()
  }
  set label(label: string){
     this.filterContext.setGroup(label);
  }

  ngOnInit() {
    
    this.labelService.getLabels().subscribe(labels => {
      
      this.labels  = labels;
      if( !this.filterContext.getGroup() ){
        this.label = this.labels[0].name;
      }
      this.refreshData()
    });
    

    this.subscribeToData();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ctxtChangeSubscription.unsubscribe();
  }



  onContextChange(){
    this.refreshData()
  }

  /**
   * Subscribe periodical timer to refresh datas
   */
  private subscribeToData(): void {
    this.timerSubscription = timer(0, this.reloadInterval).subscribe(() => this.refreshData())
  }


  removeFilter(removedfilter){

    this.filterContext.removeFilter(removedfilter)
    this.refreshData();
  }

  refreshData(){

    if(!this.filterContext.getGroup()){
      return;
    }
    let criteria: GroupStatusProbeCriteria = {};
    criteria.labels =  this.filterContext.serialiseLabels()
    criteria.groups = ["labels."+this.filterContext.getGroup(), "host", "probeId"]
    criteria.query = this.filterContext.getQuery()

    let KVState = this.filterContext.getFieldFilter("state")
    if( KVState){
      criteria.state  = KVState.value
    }
    let KVStatus = this.filterContext.getFieldFilter("status")
    if( KVStatus){
      criteria.status  = KVStatus.value
    }
    let KVType = this.filterContext.getFieldFilter("type")
    if( KVType){
      criteria.type = KVType.value
    }
    
    this.group$ =  this.probeResultService.getGroupStatusProbe(criteria)
  }

  onGroupChange(event): void{
    this.filterContext.setGroup(event.source.value);
    this.refreshData();
  }

  get filters() {
    return this.filterContext.getFilters();
  }

  /*_______________________*/



  loadLabels( serializedLabels: string): void {

      if(!serializedLabels){
        return;
      }
      let labelsTab: string[] = serializedLabels.split(',')

      labelsTab.forEach(serializedLabels => {

        if(serializedLabels.includes(":")){
          var labelTab: string[] = serializedLabels.split(":")

          if(labelTab.length == 2){
            this.filterContext.addFilter({
             "key": labelTab[0],
             "value": labelTab[1],
             "type": FilterType.LABEL,
            });       
          }
        }
      });
  }


}
   