import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { Observable } from 'rxjs';
import { NodeStatus, NodeHisto, NodeHistoQuery } from '../model/node';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private api: APIService) {
  }

  searchStatus(): Observable<NodeStatus[]>{
    return this.api.getJson(`/nodes/_status`)
  }

  searchHisto(query: NodeHistoQuery): Observable<NodeHisto[]>{

    let paramsList: string[] = [];
    let params = ""

    if(query.probeId){
      paramsList.push("probeId=" + query.probeId)
    }
  
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }

    return this.api.getJson(`/nodes/_search${params}`)
  }


}
