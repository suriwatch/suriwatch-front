import { Component, OnInit } from '@angular/core';
import { NodeStatus } from 'src/app/model/node';
import { Observable, Subscription } from 'rxjs';
import { NodeService } from '../node.service';
import { Router } from "@angular/router";
import { KVFilter } from 'src/app/label/label';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';

@Component({
  selector: 'app-view-nodes-status',
  templateUrl: './view-nodes-status.component.html',
  styleUrls: ['./view-nodes-status.component.scss']
})
export class ViewNodesStatusComponent implements OnInit {

  nodes$ : Observable<NodeStatus[]> = null;
  submitted = false;
  ctxtChangeSubscription: Subscription;

  constructor(
    public nodeService: NodeService,
    private router: Router,
    private filterContext: FilteringContextService,
  ) { 
      this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
        change => {
          this.search()
      });  
  }

  ngOnInit() {
    this.search()
  }
  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ctxtChangeSubscription.unsubscribe();
  }

  search() { 
    this.submitted = true; 
    this.nodes$ = this.nodeService.searchStatus()

  }
  selecteLabel(kvFilter: KVFilter): void{
    this.filterContext.addFilter(kvFilter); 
  }

  redirectToProbesList(labels: Map<String, String>) {
    this.router.navigate(["/probes"], { queryParams: { labels: this.serializeLabels(labels) }});
  }

  serializeLabels(labels: Map<String, String>): string {
    let paramsList: string[] = [];
    for (let key in labels) {
      paramsList.push(key + ":" + labels[key])
    }
    return paramsList.join(",")
  }

}
