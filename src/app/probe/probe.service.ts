import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { Observable } from 'rxjs';
import {Probe, SearchProbeQuery} from '../model/probe'

@Injectable({
  providedIn: 'root'
})
export class ProbeService {

  constructor(private api: APIService) {
  }

  getProbe(name: string): Observable<Probe> {
    return this.api.getJson(`/probes/${name}`)
  }

  getProbes(): Observable<Probe[]> {
    return this.api.getJson(`/probes?size=1000`)
  }


  searchProbes(query: SearchProbeQuery): Observable<Probe[]>{

    let paramsList: string[] = [];
    let params = ""
   
    if(query.size !== undefined){
      paramsList.push(`size=${query.size}`)
    }else{
      paramsList.push(`size=250`)
    }

    if(query.state !== undefined){
      paramsList.push(`state=${query.state}`)
    } 
    if(query.name !== undefined){
      paramsList.push(`name=${query.name}`)
    } 
    if(query.type !== undefined){
      paramsList.push(`type=${query.type}`)
    }  
    if(query.mode !== undefined){
      paramsList.push(`mode=${query.mode}`)
    }     
    if(query.labels !== undefined){
      paramsList.push(`labels=${query.labels}`)
    }


    if(query.q !== undefined){
      paramsList.push(`q=${query.q}`)
    }
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }

    return this.api.getJson(`/probes${params}`)
  }


  searchProbesByLabels(labels: string, query?: string): Observable<Probe[]>{

    let paramsList: string[] = [];
    let params = ""
   
    paramsList.push(`size=250`)

    if(labels !== undefined){
      paramsList.push(`labels=${labels}`)
    }
    if(query !== undefined){
      paramsList.push(`q=${query}`)
    }
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }

    return this.api.getJson(`/probes${params}`)
  }

  save(probe: Probe): Observable<Probe>{
    return this.api.postJson(`/probes`, probe);
  }

  delete(name: string): Observable<void>{
    return this.api.delete(`/probes/${name}`)
  }

}
