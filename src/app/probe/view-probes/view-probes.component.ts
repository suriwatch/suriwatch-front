import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { ProbeService } from '../probe.service';
import { Observable, Subscription, of } from 'rxjs';
import { Probe, SearchProbeQuery } from '../../model/probe';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { LabelSelectorDialog } from 'src/app/label/dialog/label-selector/label-selector.dialog';
import { RemoveConfirmDialog } from 'src/app/core/dialog/remove-confirm/remove-confirm.dialog';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';
import { KVFilter, FilterType } from 'src/app/label/label';


@Component({
  selector: 'app-view-probes',
  templateUrl: './view-probes.component.html',
  styleUrls: ['./view-probes.component.scss']
})
export class ViewProbesComponent implements OnInit, OnDestroy {

  submitted = false;
  probes$ : Observable<Probe[]> = null;
  ctxtChangeSubscription: Subscription;

  constructor(
    public dialog: MatDialog,
    private probeService: ProbeService,
    private filterContext: FilteringContextService,
  ) {
    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        this.search()
    });
  }

  ngOnInit() {
    this.search()
  }
  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.ctxtChangeSubscription.unsubscribe();
  }

  selecteLabel(kvFilter: KVFilter): void{
    this.filterContext.addFilter(kvFilter); 
  }
  
  
  selecteMode(mode: string): void{
    this.filterContext.addFilter(
      {
        "key": "mode",
        "value": mode,
        "type": FilterType.FIELD,
       });
  }


  selecteType(type: string): void{
    this.filterContext.addFilter(
      {
        "key": "type",
        "value": type,
        "type": FilterType.FIELD,
       });
  }

  selecteState(state: string): void{
    this.filterContext.addFilter(
      {
        "key": "state",
        "value": state,
        "type": FilterType.FIELD,
       });
  }

  get query():string {
    return this.filterContext.getQuery();
  }
  set query(query: string) {
     this.filterContext.setQuery(query)
  }
  get filters() {
    return this.filterContext.getFilters();
  }

  isFiltered(){
    return this.filterContext.isFiltered();
  }
  resetFilters(){
    this.filterContext.reset();
  }


  search() { 

    this.submitted = true; 

    let query: SearchProbeQuery  = {}
    query.q = this.filterContext.getQuery()
    query.labels = this.filterContext.serialiseLabels()

    let KVState = this.filterContext.getFieldFilter("state")
    if( KVState){
      query.state = KVState.value
    }
    let KVType = this.filterContext.getFieldFilter("type")
    if( KVType){
      query.type = KVType.value
    }
    let KVTMode = this.filterContext.getFieldFilter("mode")
    if( KVTMode){
      query.mode = KVTMode.value
    }
    this.probes$ = this.probeService.searchProbes(query) 

  }

  onContextChange(){
    this.search()
  }


  onDeleteProbe(probe: Probe){
    
    const dialogRef = this.dialog.open(RemoveConfirmDialog, {
      width: '500px',
      data: { entity: probe.name}
    });

    dialogRef.afterClosed().subscribe(result => {
     
      if(result){
        this.probeService.delete(probe.id).subscribe(_ => {
          this.search()
        });
      }
    });
  }


}

