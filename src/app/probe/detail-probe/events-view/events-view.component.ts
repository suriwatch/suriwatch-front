import { Component, OnInit, Input } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import { Observable } from 'rxjs';
import { StatusChangeEvent, EventQuery } from 'src/app/model/event';
import { Period } from 'src/app/model/probe-result';

@Component({
  selector: 'sw-events-view',
  templateUrl: './events-view.component.html',
  styleUrls: ['./events-view.component.scss']
})
export class EventsViewComponent implements OnInit {

  constructor(private eventService: EventService) { }
 
  events$ : Observable<StatusChangeEvent[]> = null;

  _period: Period;

  @Input()
  probeId: string

  @Input()
  set period(period: Period) {
    this._period = period
    this.search()
  }


  ngOnInit() {
    this. search()

  }

  search(){
    let query: EventQuery = {
      probeId : this.probeId,
      from: this._period.from,
      to: this._period.to,
      sort: [ { "name": "start", "order": "desc"}]
    }
    this.events$ = this.eventService.search(query)
  }

}
