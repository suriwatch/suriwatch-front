import { Component, OnInit, Input } from '@angular/core';
import { ProbeResultHistogram, HistoDateBucket } from 'src/app/model/probe-result';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'sw-status-chart',
  templateUrl: './status-chart.component.html',
  styleUrls: ['./status-chart.component.scss']
})
export class StatusChartComponent implements OnInit {
  
  // options
  showXAxis = false;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Time';
  showYAxisLabel = false;
  yAxisLabel = 'Results';
  barPadding = 1
  animations = true;

  colorScheme ={
    domain: ['#728ef7', '#DA2F37']
  }; 
  customColors = [
    {  name: 'KO', value: '#DA2F37'},
    {  name: 'OK', value: '#728ef7'}
  ];

  single: any[];
  multi: any[];
 
  view: any[] = undefined;//[800, 100];

  
  public _histogram:  ProbeResultHistogram

  @Input()
  set histogram(histogram: ProbeResultHistogram) {
    this._histogram = histogram;
    this.loadData()
  }

  ngOnInit() {
  }

  private loadData(){
    this.multi = this.probeResultsToData(this._histogram)
  }

  private probeResultsToData(result: ProbeResultHistogram) : any[]{

    var datas = []    
        
    if( result == undefined || result.data == undefined){
      return datas;
    }

    result.data.forEach(val => {
      datas.push(this.probeBucketToData(val))
    });
    return datas
  }

  private probeBucketToData(val: HistoDateBucket): any{
  
    var entry = {
      "name": val.date,
      "series": []
    }

    if( "status" in val){
      for (let key in val.status) {  
        entry.series.push(
          {
            "name": key,
            "value": val.status[key]  
          }
        )
      }  
    }
    return entry;
  }

}
