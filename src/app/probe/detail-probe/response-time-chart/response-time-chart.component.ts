import { Component, OnInit, Input } from '@angular/core';
import { ProbeResultHistogram, HistoDateBucket } from 'src/app/model/probe-result';

@Component({
  selector: 'sw-response-time-chart',
  templateUrl: './response-time-chart.component.html',
  styleUrls: ['./response-time-chart.component.scss']
})
export class ResponseTimeChartComponent implements OnInit {

  constructor() { }


  @Input()
  public _histogram:  ProbeResultHistogram

  colorScheme ={
    domain: ['#5684FE', '#DA2F37']
  }; 
  customColors = [
    {  name: 'Max', value: '#DA2F37'},
    {  name: 'Avg', value: '#5684FE'}
  ];

  single: any[];
  multi: any[] = [];
  view: any[] = undefined;

  // options
  showXAxis = false;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Time';
  showYAxisLabel = true;
  yAxisLabel = 'Total (ms)';
  timeline = true;



  // line, area
  //autoScale = true;
  autoScale = false;

  @Input()
  set histogram(histogram:  ProbeResultHistogram) {
    this._histogram = histogram;
    this.loadData()
  }

  ngOnInit() {
  }

  private loadData(){
    this.multi = this.histogramToData(this._histogram)
  }


  private histogramToData(histo: ProbeResultHistogram): any[]{
    var datas = []    
        
    if( histo == undefined || histo.data == undefined){
      return datas;
    }
    var entryMax = {
      "name": "Max",
      "series": []
    }

    var entryAvg = {
      "name": "Avg",
      "series": []
    }

    histo.data.forEach(histoData => {
      if(histoData.totalTime.max != undefined){
        entryMax.series.push(
          {
            "name": histoData.date,
            "value": histoData.totalTime.max
          }
        );
      }
          
      if(histoData.totalTime.avg != undefined){
        entryAvg.series.push(
          {
            "name": histoData.date,
            "value": histoData.totalTime.avg
          }
        ); 
      }

    });

    datas = [entryAvg, entryMax]
    console.log(datas)
    return datas
  
  }

}
