import { Component, OnInit, OnDestroy } from '@angular/core';

import { ProbeService } from '../probe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { timer, Observable, Subscription } from 'rxjs';
import { Probe } from '../../model/probe';
import { ProbeResultService } from '../../probe-results/probe-result.service';
import { NodeHisto, NodeHistoQuery } from 'src/app/model/node';
import { NodeService } from 'src/app/node/node.service';
import { Period } from 'src/app/model/probe-result';
import { PeriodSelectorDialog } from 'src/app/core/dialog/period-selector/period-selector.dialog';
import { MatDialog } from '@angular/material';
import { DateAnalyser } from '../../core/period-utils';

@Component({
  selector: 'app-detail-probe',
  templateUrl: './detail-probe.component.html',
  styleUrls: ['./detail-probe.component.scss']
})
export class DetailProbeComponent implements OnInit, OnDestroy {

  private timerSubscription: Subscription = null

  histogram: any = null;

  // Graph reload interval in milliseconds
  reloadInterval  = 5000;

  public probeId: string
  private nodeHisto: NodeHisto = null;

  public probe: Probe = null
  public probeName: string = "Probe detail"
  public nodesHisto$: Observable<NodeHisto[]> = null

  public period :Period = null;
  private interval = "30s"


  customPeriodOption =  { from: null, to: null, name: "custom"}
  customPeriod = undefined
  public periods: Period[] = [
    { from: "now-10m", to: "now", name: "Last 10m"},
    { from: "now-1h",  to: "now", name: "Last 1h"},
    { from: "now-24h", to: "now", name: "Last 24h"},
    { from: "now-7d",  to: "now", name: "Last 7d"}
  ];
  public intervals: string[] = ["30s","5m", "10m", "1h", "1d"];
  
  // ACL
  public showResponseTime = false;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private probeService: ProbeService,
    private probeResultService: ProbeResultService,
    private nodeService: NodeService,
    public dialog: MatDialog,
    ) {
    //Object.assign(this, {single, multi})   
    Object.assign(this, { multi:[] })

    this.route.params.subscribe(params => {
      this.probeId = params["id"];
      this.loadProbe(this.probeId);
      this.loadNodeHisto(this.probeId);
      this.subscribeToData()
    });  
  }

  ngOnInit() {
    this.period = this.periods[0];
  }

  ngOnDestroy() {
    this.timerSubscription.unsubscribe();
  }

  public onPeriodChange(period: Period){

    if(period.name == "custom"){
      const dialogRef = this.dialog.open(PeriodSelectorDialog, {
        width: '500px',
        data: { from: new Date(), to: new Date()}
      });
  
    
      dialogRef.afterClosed().subscribe(result => {

        if(result){
          this.period = {
            from:  Math.round(result.from.getTime())+"",
            to:   Math.round(result.to.getTime())+"" ,
            name: result.from +"-"+result.to
          }
          this.customPeriod = this.period
          this.refreshData()
        }
      });
    } else{
      this.customPeriod = undefined
    }
  }

  /**
   * Retrieve the probe form the API and load the probe form 
   *
   * @param probeId 
   * 
   */
  loadProbe(probeId: string){

    // Getting http probe
    let probe$ = this.probeService.getProbe(probeId);

    probe$.subscribe(probe => {
      this.probe = probe
      this.probeName = probe.name
      this.showResponseTime = probe.type == 'http'
    })
  }

  /**
   * Retrieve the probe form the API and load the probe form 
   *
   * @param probeId 
   * 
   */
  loadNodeHisto(probeId: string){


    let query: NodeHistoQuery = {
      probeId:  probeId
    }

    // Getting http probe
    this.nodesHisto$ = this.nodeService.searchHisto(query);

  }
  


  refreshData(){
    let dateAnalyser = new DateAnalyser()

    var nodeName: string  = undefined;
    if(this.nodeHisto){
      nodeName = this.nodeHisto.name
    }
    
    let intervalMin = this.computeInterval(dateAnalyser.getDate(this.period.from), dateAnalyser.getDate(this.period.to))
    let intervalAsked = dateAnalyser.getDurationAsSeconds(this.interval)
    
    // Born is interval is to small
    let interval = intervalAsked
    if(intervalAsked <  intervalMin){
      interval = intervalMin
    }
    //
    this.loadProbeDataHist(this.probeId, nodeName, this.period.from, this.period.to, interval+"s")
  }

  /**
   * Retrieve the probe form the API and load the probe form 
   *
   * @param {string} [probeId] The probe who own datas
   * 
   */
  loadProbeDataHist(probeId: string, agent?: string, from?: string, to?: string, interval?: string ){

      this.probeResultService.getProbeHisto(probeId, agent, from, to, interval).subscribe(
          result => { 
            this.histogram = result;
          }
      ); 
  }


  private subscribeToData(): void {
    this.timerSubscription = timer(0, this.reloadInterval).subscribe(() => this.refreshData())
  }


  public onNodeHistoChange(node: NodeHisto): void {
    this.refreshData()
  }
  

  private computeInterval(from: Date, to: Date): number{

      var maxBuckets = 200
      var duration = to.getTime() - from.getTime()


      var defaultInterval = Math.round(duration / maxBuckets)

      var value = Math.round(defaultInterval/1000);
      value = value -(value % 5)

      if(value< 1){
        value =1
      }
      return value 

  }
}
