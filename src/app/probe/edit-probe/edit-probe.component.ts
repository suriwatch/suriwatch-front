import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Probe, NetworkProbeRequest, HttpProbeRequest, HttpProbeResponse } from '../../model/probe';
import { ProbeService } from '../probe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { map, filter, flatMap } from 'rxjs/operators';
import { LabelService } from 'src/app/label/label.service';
import { Label } from 'src/app/label/label';


@Component({
  selector: 'app-edit-probe',
  templateUrl: './edit-probe.component.html',
  styleUrls: ['./edit-probe.component.scss']
})
export class EditProbeComponent implements OnInit {

  probeForm: FormGroup;
  
  probe$ : Observable<Probe> = null;
  probeId: string = null;
  selectedTab: string = 'httpRequestTab'
  form_submited: boolean = false

  labelKeys: string[]
  labels: Label[]

  filteredLabelKeys: Observable<string[]>
  filteredLabelValues: Observable<string[]>
  
  updateAutoCompleteLabelKeys(value){
    this.filteredLabelKeys = of(this.labelKeys).pipe(
      map(
        labelKeys => {
          return labelKeys.filter(labelKey => labelKey.startsWith(value))
        }
      )
    )
  }

  updateAutoCompleteLabelValues(name: string, value: string){

    console.log(name, value)
    this.filteredLabelValues =  of(this.labels)
        .pipe(
          map(labels =>  {
            return labels.filter(l => l.name === name)
          }
        )
      )
      .pipe(
        map( labels => {
            let values = []
            labels.forEach(label => {
              let valuesOk = label.values.filter(labelKey => labelKey.startsWith(value))
              values= values.concat(valuesOk);
            });
            return values; 
          }
        )
      )
  }


  constructor(
    private probeService: ProbeService,
    private labelService: LabelService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router) { 

    this.route.params.subscribe(params => {
      this.probeId = params["id"];
      this.probeForm = this.getInitializedProbeForm()

      const probe = {} as Probe;

      if( this.probeId){
        this.loadProbe(this.probeId);
      }else{
        this.probe$ = of(probe);
      }
    });  

    this.initLabelsKeys();
  }

  initLabelsKeys(){
    this.labelService.getLabels().pipe(map(labels => {
      // Record labels
      this.labels  = labels;

      // Extrat label keyss
      let res = []
      if(labels){
        labels.forEach(label => {
          res.push(label.name)
        });
      }
      return res;
    })).subscribe(labelKeys => {
      this.labelKeys = labelKeys;
    })
  }
  
  ngOnInit() {
    this.filteredLabelKeys =  of(this.labelKeys)
  }

  /**
   * Initialiaze probe formulaire and attach form listeners
   */
  getInitializedProbeForm(){

    let formGroup =  this.formBuilder.group({
      name: new FormControl(null, Validators.required ),
      description: new FormControl(null ),
      type: new FormControl('http', Validators.required),
      state: new FormControl('STARTED', Validators.required),
      labels: new FormArray([new FormGroup({
        name: new FormControl(null, Validators.required),
        value: new FormControl(null, Validators.required)
      })], Validators.required),
      interval: new FormControl(30),
      mode: new FormControl('CLUSTER', Validators.required),

      httpProbeConfig: new FormGroup({
        request: new FormGroup({
          url: new FormControl(),
          verb: new FormControl('GET'),
          content: new FormControl(),
          timeout: new FormControl(1000), 
          headers: new FormArray([]),
        }),
        response: new FormGroup({
          httpCode: new FormControl(200), // Validators.min(0)),
          keyword: new FormControl(),
        })
      }),

      networkProbeConfig: new FormGroup({
        request: new FormGroup({
          port: new FormControl(),
          target: new FormControl(),
          protocol: new FormControl(),
          timeout: new FormControl() 
        }),
      })
    });

    // Registe on change listeners
    this.subscribeTypeChanges(formGroup);
    return formGroup;
  }


  /**
   * Remove all validator for the give group
   * @param group 
   */
  removeFormGroupValidators(group: FormGroup){
    Object.keys(group.controls).forEach(key => {
        group.controls[key].setValidators(null);
        group.controls[key].updateValueAndValidity();
    });
  }
  /**
   * Add HTTP Probe form validators
   */
  addHTTPProbeValidators(){
    let formGroup: FormGroup = <FormGroup>  this.probeForm.get('httpProbeConfig.request');
    formGroup.controls['url'].setValidators(Validators.required)
    formGroup.controls['timeout'].setValidators(Validators.min(1))
  }

  /**
   * Add Network Probe form validators
   */
  addNetworkProbeValidators(){
      // Attach validator form Network
      let formGroup: FormGroup = <FormGroup>  this.probeForm.get('networkProbeConfig.request');
      formGroup.controls['target'].setValidators(Validators.required)
      formGroup.controls['port'].setValidators(Validators.min(1))
      formGroup.controls['timeout'].setValidators(Validators.min(1))
  }

  /**
   * Subsribe probe type change.
   * 
   * Enable / Disable validators according selected probe type
   *
   * @param probeForm 
   */
  subscribeTypeChanges(probeForm: FormGroup){
    
    // initialize value changes stream
    const changes$ = probeForm.get('type').valueChanges;

    // subscribe to the stream
    changes$.subscribe(probeType => {
      if (probeType === 'http') {
        
        // Attach http probe form validators 
        this.addHTTPProbeValidators()

        // Remove validators for Network Probe
        this.removeFormGroupValidators(<FormGroup> probeForm.get('networkProbeConfig.request'))
      }

      if (probeType === 'network'){

        // Attach network probe form validators 
        this.addNetworkProbeValidators()
        
        // Remove validators for HTTP Probe
        this.removeFormGroupValidators(<FormGroup> probeForm.get('httpProbeConfig.request'))
        this.removeFormGroupValidators(<FormGroup> probeForm.get('httpProbeConfig.response'))
      }
    });
  }

  /**
   * Retrieve the probe form the API and load the probe form 
   *
   * @param probeId 
   * 
   */
  loadProbe(probeId: string){

    // Getting http probe
    this.probe$ = this.probeService.getProbe(probeId);

    this.probe$.subscribe(probe => {
      this.modelToForm(probe)

      this.route.url.subscribe(url => {
        if (url[url.length - 1].path === 'copy') {
          this.probeId = null;
          this.probeForm.patchValue({ name: this.probeForm.get('name').value + '-copy' });
        }
      })
    })
  }




  buildEntry(): FormGroup {
    return new FormGroup({
      name: new FormControl('',Validators.required),
      value: new FormControl()})
  }


  initFormGroupHeader(header ): FormGroup{
    let fg = this.buildEntry();
    fg.patchValue(header)
    return fg
  }



  modelToForm(probe: Probe){


    // Create a patch object and apply it (/!\ Can't use Object.assign form deep copy, )
    var formPatch = JSON.parse(JSON.stringify(probe));

    // Don't want to apply patchValue on labels 
    delete formPatch["labels"]; 

    if(probe.type == 'http'){

      delete formPatch["networkProbeConfig"]
      if( probe.httpProbeConfig.request != null){

        // Don't patch header. Deal with it manually
        delete formPatch.httpProbeConfig.request["headers"]

        if( probe.httpProbeConfig.request.headers != null){

          // Initialise HTTP probe request headers form
          let controlArray = <FormArray>this.probeForm.get('httpProbeConfig.request.headers')           
          probe.httpProbeConfig.request.headers.forEach(header => {
            controlArray.push(this.initFormGroupHeader(header));
          });
        }

      }

    }else if(probe.type == 'network' ){
      delete formPatch["httpProbeConfig"]
    }

    this.probeForm.patchValue(formPatch)

    // Initialize probe labels form   
    let labelControlArray = <FormArray>this.probeForm.get('labels')   

    if(labelControlArray.length > 0){
      labelControlArray.removeAt(0)
    }

    if(probe.labels){
      for (let key in probe.labels) {
        labelControlArray.push(  new FormGroup({
          name: new FormControl(key,Validators.required),
          value: new FormControl( probe.labels[key])
        }))
      }
    }

  }

  /**
   * 
   */
  formToModel(): Probe{
    const result: Probe = Object.assign({}, this.probeForm.value);
    result.id = this.probeId 

    if( result.labels){
      var labelsObj = {}
      result.labels.forEach(label => {

        if(Array.isArray((<any> label).value)){
          labelsObj[ (<any> label).name ] =  (<any> label).value 

        }else if(typeof (<any> label).value  === 'string'){
          labelsObj[ (<any> label).name ] = (<any> label).value .split(","); 
        }

      });
      result.labels = labelsObj
    }

    if(result.interval){
      result.interval  = +result.interval
    }


    if(result.type == 'http'){
      result.networkProbeConfig = null;
      
      let request: HttpProbeRequest =  result.httpProbeConfig.request
      request.timeout  = +request.timeout

      let response: HttpProbeResponse =  result.httpProbeConfig.response
      response.httpCode = +response.httpCode


    }else if( result.type == 'network'){
      result.httpProbeConfig = null

      let request: NetworkProbeRequest =  result.networkProbeConfig.request
      request.timeout = +request.timeout
      request.port = +request.port    
    }
    return result
  }



  submit() {
    console.log("Template-driven form submitted: ", this.probeForm);
    this.form_submited = true 

    if( this.probeForm.valid ) {
      let probeToSave = this.formToModel()


      console.log(probeToSave)
      this.probeService.save(probeToSave).subscribe(probe => {
        this.router.navigate(["probes"]);
      });
    }
  }


  selectHTTPTab(tab: string){
    this.selectedTab = tab
  }
}
