import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from 'protractor';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  conf: Config;

  constructor(private http: Http) { }

  getConfiguration(): Observable<Config> {
    if (this.conf == null) {
      return this.http.get("/assets/conf.json").pipe(map(response => {
        this.conf = <Config>response.json();
        return this.conf;
      }));
    } else {
      return of(this.conf);
    }
  }

}
