//https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html



export class DateAnalyser {

    private dateRegExp = new RegExp('(?<date>now|\\d+)(?<shift>((?<operator>[+-])(?<duration>\\d+)(?<unit>[y|M|w|d|h|H|m|s])))?'); 
  
    private durationRegExp = new RegExp('(?<operator>[+-])?(?<duration>\\d+)(?<unit>[y|M|w|d|h|H|m|s])'); 



    private _parseDurationExpr(value: string): DurationExpression{

        let matchObj = this.durationRegExp.exec(value);
        let groups:any =  matchObj["groups"]

        let res:DurationExpression  = {
            operator : groups.operator? groups.operator: '+',
            duration : groups.duration,
            unit :groups.unit,
        } 
 
        return res;
    }

    private _durationExpToSeconds(value: DurationExpression): number{
        
        let res:number = +value.duration;

        if(value.operator == '-'){
            res = -res;
        }
        if(value.unit == 'y'){
            res = res * 60*60*24*7*365

        }else if(value.unit == 'M'){
            res = res * 60*60*24*7*30

        }else if(value.unit == 'w'){
            res = res * 60*60*24*7
        }else if(value.unit == 'd'){

            res = res * 60*60*24

        }else if(value.unit == 'h' ||  value.unit == 'H'){
            // apply hours shifting
            res = res * 60*60 

        }else if(value.unit == 'm'){
            // apply minuts shifting
            res = res * 60

        }else if(value.unit == 's'){
            // Nothing to do
        }
        

        return res;

    }

    private _parseDateExpr(value: string): DateExpression{
       
        let res:DateExpression = {
            date : ""  
        }

        let matchObj = this.dateRegExp.exec(value);
        let groups:any =  matchObj["groups"]

        res.date = groups.date;

        if(groups.shift){
            res.shift = {
                operator : groups.operator,
                duration : groups.duration,
                unit :groups.unit,
            } 
        }

        return res;
    }

    private _dateExprToDate(expr: DateExpression): Date{

        let res = null
        if(expr.date == 'now'){
            res = new Date()
        }else{
            let millis = +expr.date
            res = new Date(millis)
        }

        if(expr.shift){

            let shift =  +expr.shift.duration
            if(expr.shift.operator == '-'){
                shift = -shift; 
            }

            if(expr.shift.unit == 'y'){
                res.setFullYear(res.getFullYear()+shift);

            }else if(expr.shift.unit == 'M'){
                res.setMonth(res.getMonth()+shift);

            }else if(expr.shift.unit == 'w'){

            }else if(expr.shift.unit == 'd'){
                // apply days shifting
                res.setDate(res.getDate()+shift);

            }else if(expr.shift.unit == 'h' ||  expr.shift.unit == 'H'){
                // apply hours shifting
                res.setHours(res.getHours()+shift);

            }else if(expr.shift.unit == 'm'){
                // apply minuts shifting
                res.setMinutes(res.getMinutes()+shift);

            }else if(expr.shift.unit == 's'){
                res.setSeconds(res.getSeconds()+shift);
            }
        }

        return res;
    }


    private _printExpression(exp: DateExpression){
        console.log("Date :" + exp.date)
        if(exp.shift){
            this._printDuration( exp.shift)
        }
    }

    
    private _printDuration(exp: DurationExpression){
        console.log("Operator :" + exp.operator)
        console.log("Duration :" + exp.duration)
        console.log("unit :" + exp.unit)

    }

    public getDurationAsSeconds(value: string): number {
        
        let exp = this._parseDurationExpr(value)
        return this._durationExpToSeconds(exp)
    }
    public getDate(value: string): Date {
        
        let exp = this._parseDateExpr(value)
        return this._dateExprToDate(exp)
    }
}

export interface DateExpression{
    date: string,
    shift?: DurationExpression
}

export interface DurationExpression{
    operator?: string,
    duration: string,
    unit: string,
}

