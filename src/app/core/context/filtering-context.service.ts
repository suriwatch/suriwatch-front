import { Injectable, OnInit } from '@angular/core';
import {  Subject } from 'rxjs';
import { KVFilter, FilterType } from 'src/app/label/label';
import { Location } from '@angular/common';


export class ContextChangeEvent{
    type: string
    emitter?: string
}

@Injectable({
  providedIn: 'root'
})
export class FilteringContextService  {

    
    private ctxtChange = new Subject<ContextChangeEvent>();

    ctxtChange$ = this.ctxtChange.asObservable();
    
    private group: string = undefined;
    private query: string = undefined;
    private filters: KVFilter[] = []
    
    constructor( ) {
    }


    public setQuery(query: string, emitter?: string): void{
        this.query = query;
        this.ctxtChange.next({
            type: "query",
            emitter: emitter,
        });
    }
    public getQuery(): string {
        return this.query;
    }   
    
    
    public setGroup(group: string, emitter?: string): void{
        this.group = group;
        this.ctxtChange.next({
            type: "group",
            emitter: emitter,
        });
    }
    public getGroup(): string {
        return this.group;
    }


    public isFiltered(): boolean{
        return this.filters.length > 0 || (this.query !== undefined && this.query !== "")
    }


    public addFilter(filter: KVFilter, emitter?: string): void{
        this.filters = this.filters.filter(f => filter.key !== f.key )
        this.filters.push(filter)
        this.ctxtChange.next({
            type: "filters",
            emitter: emitter,
        })
    }
    
   
    public removeFilter(removedfilter, emitter?: string){
        this.filters = this.filters.filter(f => f.key != removedfilter.key || f.value != removedfilter.value)
        this.ctxtChange.next({
            type: "filters",
            emitter: emitter,
        });
    }

    public getFilters():  KVFilter[]{
        return this.filters;
    }


    public serialiseLabels(): string {
        let labels: string[] = [];
        for (let filter of this.filters) {
            if(filter.type == FilterType.LABEL){
                labels.push(filter.key + ":" + filter.value);
            }
        }

        if (labels.length > 0){
            return labels.join(",")
        }  
        return "";      
    }

    public getFieldsFilters(): KVFilter[] {
        return this.filters.filter((fld)=> { return fld.type === FilterType.FIELD})
    }

    public getFieldFilter(name: string): KVFilter {
        let fields =  this.filters.filter((fld)=> { return fld.key === name && fld.type === FilterType.FIELD})

        if( fields.length > 0 ){
            return fields[0]
        }
    }

    public reset(emitter?: string){
        this.filters = []
        this.query = undefined
        this.group = undefined
        this.ctxtChange.next({
            type: "reset",
            emitter: emitter,
        });
    }

    public setContext( group: string, query: string, filters: KVFilter[], emitter?: string){
        this.filters = filters
        this.query = query
        this.group = group
        this.ctxtChange.next({
            type: "init",
            emitter: emitter,
        });
    }

    public getCtxtParams(): any{

        let params = {}

        let serializedLabels = this.serialiseLabels()
        if( serializedLabels && serializedLabels != ""){
            params['labels'] = serializedLabels;
        }
       
        let group = this.getGroup()
        if( group && group != ""){
            params['grp'] = group;
        }
        return params

    }
 

}
