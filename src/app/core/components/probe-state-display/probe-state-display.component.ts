import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sw-probe-state',
  templateUrl: './probe-state-display.component.html',
  styleUrls: ['./probe-state-display.component.scss']
})
export class ProbeStateDisplayComponent implements OnInit {

  constructor() { }

  @Input("state")
  public state: string = null

  @Output("selected") 
  public selected = new EventEmitter<string>();
  
  ngOnInit() {
  }
  select() {
    this.selected.emit(this.state);
  }
}
