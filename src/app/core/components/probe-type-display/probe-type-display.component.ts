import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sw-probe-type',
  templateUrl: './probe-type-display.component.html',
  styleUrls: ['./probe-type-display.component.scss']
})
export class ProbeTypeDisplayComponent implements OnInit {

  constructor() { }

  @Input("type")
  public type: string = null

  @Output("selected") 
  public selected = new EventEmitter<string>();
  
  ngOnInit() {
  }
  select() {
    this.selected.emit(this.type);
  }
}
