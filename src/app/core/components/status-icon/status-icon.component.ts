import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'sw-status-icon',
  templateUrl: './status-icon.component.html',
  styleUrls: ['./status-icon.component.scss']
})
export class StatusIconComponent implements OnInit {

  constructor() { }

  @Input() status: string;

  @Output("selected") 
  public selected = new EventEmitter<string>();

  ngOnInit() {
  }

  select() {
    this.selected.emit(this.status);
  }

}
