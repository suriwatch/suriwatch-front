import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sw-probe-mode',
  templateUrl: './probe-mode-display.component.html',
  styleUrls: ['./probe-mode-display.component.scss']
})
export class ProbeModeDisplayComponent implements OnInit {

  constructor() { }

  @Input("mode")
  public mode: string = null

  @Output("selected") 
  public selected = new EventEmitter<string>();
  
  ngOnInit() {
  }
  select() {
    this.selected.emit(this.mode);
  }
}
