import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Period } from 'src/app/model/probe-result';
import { DataRowOutlet } from '@angular/cdk/table';

@Component({
  selector: 'sw-period-picker',
  templateUrl: './period-picker.component.html',
  styleUrls: ['./period-picker.component.scss']
})
export class PeriodPickerComponent implements OnInit {

  public period : DatePeriod;

  @Output("changed") 
  public changed = new EventEmitter<DatePeriod>();
  
  @Input("from")
  set from(from: Date){
    this.period.from = from
  }

  @Input("to")
  set to(to: Date){
    this.period.to = to
  }
 
  constructor() {
    this.period = { from: new Date(), to: new Date() }
   }

  ngOnInit() {
  }

  public dateChangeFrom(date: Date): void{
    this.period.from = date;
    this.changed.emit(this.period);
  }
  public dateChangeTo(date: Date): void{
    this.period.to = date;
    this.changed.emit(this.period);
  }
}

export interface DatePeriod{
    from: Date,
    to: Date
}

