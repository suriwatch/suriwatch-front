import { Component, OnInit,  Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sw-datetime-picker',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.scss']
})
export class DatetimePickerComponent implements OnInit {

  constructor() { }

  public date = new Date();
  public hours = "";
  public minutes = ""; 

  private _datetime: Date = new Date()

  @Input("datetime")
  public set datetime(datetime: Date){
    this._datetime = datetime;
    this. _dateToForm();
  }

  @Output("dateChange") 
  public _dateChange = new EventEmitter<Date>();

  private _dateToForm(){
    this.date = this._datetime 
    this.hours = this._datetime.getHours().toString()
    this.minutes = this._datetime.getMinutes().toString()
  }

  private _formToDate(){
    this._datetime = this.date 
    this._datetime.setMinutes(parseInt(this.minutes));
    this._datetime.setHours(parseInt(this.hours)); 
    this._dateChange.emit(this._datetime)


  }

  ngOnInit() {
  }

  public dateChange(event: Date){
    this.date = event;
    this._formToDate();
  }

}
