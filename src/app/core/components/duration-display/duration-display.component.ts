import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sw-duration-display',
  templateUrl: './duration-display.component.html',
  styleUrls: ['./duration-display.component.scss']
})
export class DurationDisplayComponent implements OnInit {

  private _duration: number;

  @Input("duration")
  set duration(d: number){
    this._duration = d
  }

  constructor() { }

  ngOnInit() {
  }
  
  getDurationLabel(): string{
    var duration = this.getDuration()
    if(duration == null){
      return ""
    }
    
    var res = "";
    if( duration.seconds > 0){
      res += duration.seconds+"s" ;
    }
    if( duration.minutes > 0){
      res =  duration.minutes+"min " + res;
    }
    if( duration.hours > 0){
      res =  duration.hours+"h " + res;
    }
    return res;

  }
  getDuration(): Duration{


    if(!this._duration|| this._duration <= 0){
      return null
    }

    var d :Duration = {}

    let rest = this._duration

    // Calcul secondss
    d.seconds = rest % 60; 
    rest =  Math.floor(rest / 60);
    if(rest <= 0){  return d; }

    d.minutes = rest % 60
    rest =  Math.floor(rest / 60);
    if(rest <= 0){  return d; }

    d.hours = rest % 24
    rest  =  Math.floor(rest / 24);
    if(rest <= 0){  return d; }

    d.jours = rest

    return d;  
  }
}

interface Duration{
  seconds? : number
  minutes? : number
  hours? : number
  jours? : number
}