import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sw-event-status-display',
  templateUrl: './event-status-display.component.html',
  styleUrls: ['./event-status-display.component.scss']
})
export class EventStatusDisplayComponent implements OnInit {

  @Input("status") 
  status: string;

  constructor() { }

  ngOnInit() {
  }
 


}
