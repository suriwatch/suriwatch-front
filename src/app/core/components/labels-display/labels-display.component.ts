import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Label, KVFilter, FilterType } from 'src/app/label/label';


@Component({
  selector: 'sw-labels',
  templateUrl: './labels-display.component.html',
  styleUrls: ['./labels-display.component.scss']
})
export class LabelsDisplayComponent implements OnInit {

  constructor() { }

  @Input("labels")
  public labels: any = null

  @Output("selected") 
  public selected = new EventEmitter<KVFilter>();
  
  ngOnInit() {
  }

  select(key: string, value: string) {
    this.selected.emit({ "key" : key, "value":value, "type": FilterType.LABEL});
  }

  public asArray(elt){
    if(Array.isArray(elt)){
      return elt
    }
    return [elt]
  }
}
