import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import { ConfigService } from '../config/config.service';
import { map, catchError, finalize, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class APIService {

  callCount: number = 0;

  isRequesting: boolean;

  constructor(private http: Http, private configService: ConfigService) { }


  private onRequestStarted() {
    this.callCount++;
    this.isRequesting = this.callCount > 0;
  }

  private onRequestStopped() {
    this.callCount--;
    this.isRequesting = this.callCount > 0;
  }

  delete(uri: string, ifnull: any = null): Observable<any> {
    this.onRequestStarted();
    return this.configService.getConfiguration().pipe(mergeMap(conf => {
      return this.http.delete(conf.api_url + uri)
        .pipe(catchError(e => this.handleError(e)))
        .pipe(finalize(() => this.onRequestStopped()));
    }))
  }

  getJson(uri: string, ifnull: any = null): Observable<any> {
    this.onRequestStarted();
    return this.configService.getConfiguration().pipe(mergeMap(conf => {
      return this.http.get(conf.api_url + uri)
        .pipe(map(response => response.arrayBuffer().byteLength > 0 ? response.json() : ifnull))
        .pipe(catchError(e => this.handleError(e)))
        .pipe(finalize(() => this.onRequestStopped()));
    }))
  }

  postJson(uri: string, data: any = "", ifnull: any = null): Observable<any> {
    this.onRequestStarted();
    return this.configService.getConfiguration().pipe(mergeMap(conf => {
      return this.http.post(conf.api_url + uri, data)
        .pipe(map(response => response.arrayBuffer().byteLength > 0 ? response.json() : ifnull))
        .pipe(catchError(e => this.handleError(e)))
        .pipe(finalize(() => this.onRequestStopped()));
    }))
  }



  handleError(e: Response): Observable<any> {

    if (e.status === 403) {
      const msg = "You are not allowed to perform this action!";
      alert(msg)
      return Observable.throw(msg);
    } else if (e.status === 400) {
      const msg = `Invalid action!<br>${e.toString()}`;
      alert(msg)
      return Observable.throw(msg);
    } else if (e.status === 404) {
      const msg = `Resource not found!<br>${e.toString()}`;
      alert(msg)
      return Observable.throw(msg);
    } else if (e.status === 0) {
      const msg = "Oups, something wrong happen !";
      alert(msg)
      return Observable.throw(msg);
    } else {
      const msg = `API error (HTTP ${e.status})!<br>${e.toString()}`;
      alert(msg)
      return Observable.throw(msg);
    }
  }

}
