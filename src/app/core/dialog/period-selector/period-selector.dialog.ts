import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {  DatePeriod } from '../../components/period-picker/period-picker.component';




@Component({
  selector: 'sw-period-selector-dialog',
  templateUrl: './period-selector.dialog.html',
  styleUrls: ['./period-selector.dialog.scss']
})
export class PeriodSelectorDialog implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PeriodSelectorDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DatePeriod
    ) {
  }

  ngOnInit(){
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public updatePeriod(period: DatePeriod ){
    this.data = period;
  }

}