import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


export interface ConfirmDialogData {
  entity : string 
}

@Component({
  selector: 'sw-remove-confirm-dialog',
  templateUrl: './remove-confirm.dialog.html',
  styleUrls: ['./remove-confirm.dialog.scss']
})
export class RemoveConfirmDialog implements OnInit {



  constructor(
    public dialogRef: MatDialogRef<RemoveConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData
    ) {
  }

  ngOnInit(){
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}