import { Probe } from "./probe";
import { ProbeResult } from "./probe-result";

export interface StatusPage {
    id:string;
    name: string;
    creation: string;
    update: string; 
    labels?: any;
}


export interface SearchStatusPageQuery{
    size?: number;
    name?: string;
}

export interface StatusPageStatus{
    statusPage: StatusPage
    probes: Probe[]
    availabilities: ProbeAvailability[]
    results: ProbeResult[]
}

export interface ProbeAvailability{
    probeId: string
    ratio: Number
    buckets: ProbeAvailabilityBucket[]
}
export interface ProbeAvailabilityBucket{
    key: string
    ratio: Number
}

