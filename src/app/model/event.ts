export interface StatusChangeEvent{
    type:      string  
    date:      string;       
    reason:    string     
	probeId?:  string            
    duration:  any 
    status?:   string;       
    labels?:   any;
}

export interface EventQuery {

    probeId?: string
    labels?: string
    from?: string
    to?: string
    sort?: SortValue[]
}

export interface SortValue {
	name:  string
	order: string
}