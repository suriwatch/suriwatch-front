export interface NodeStatus{

	node: string           
	host:     string            
    date:      string;    
    startedAt: string;    
    labels?:   any;
    probesCount: number;
    mode: string
}

export interface NodeHisto{
    name: string  
}

export interface NodeHistoQuery{
    probeId?: string  
}