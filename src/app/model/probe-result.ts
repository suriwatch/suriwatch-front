export interface ProbeLastResultCriteria {

    query?: string
    status?: string
    host?:  string
    labels?: string
    type?: string
}

export interface GroupStatusProbeCriteria {

    query?: string
    status?: string
    state?: string
    host?:  string
    labels?: string
    type?: string
    groups?: string[]
}

export interface ProbeResultGroup {

    name: string;
    status: string;
    groups?:  ProbeResultGroup[];
    datas?: ProbeResult[];
}

export interface ProbeResult {
    probeId:string;
    name: string;
    status: string;
    duration: number;
    date: string;
    host: string;
    type: string;
    infos   : string;
    labels?: any;
    HTTPStats?: HTTPStats;
}

export interface HTTPStats {
    TCPConnection?: number;
    TLSHandshake?: number;
    serverProcessing?: number;
    total?: number;
}

export interface Period{
    name: string;
    from: string;
    to: string;
}



export interface ProbeResultHistogram{
    data: HistoDateBucket[];
}
export interface HistoDateBucket {
    status: Object
    totalTime: StatsMesure
    date: string
}

export interface StatsMesure {
    avg: number
    max: number
}