export interface Probe {

    id: string
    name: string
    type: string
    interval: number
    state: string
    description: string
    labels?: any
    mode?: string
    visibility?: string
    networkProbeConfig?: NetworkProbeConfig
    httpProbeConfig?: HttpProbeConfig
}

export interface HttpProbeConfig{
    request: HttpProbeRequest
    response: HttpProbeResponse
}


export interface HttpProbeRequest{

    url: string;
    verb: string;
    content: string;
    timeout: number
    headers: any[];
}

export interface HttpProbeResponse{

    httpCode: number;
    keyword: string;
}

export interface NetworkProbeConfig{
    request: NetworkProbeRequest
}

export interface NetworkProbeRequest{
    timeout: number
    target:string
    port: number
    protocol: string
}




export interface SearchProbeQuery {

    labels?: string
    name?: string
    state?: string
    mode?:string
    type?: string
    q?: string
    size?: number
}

