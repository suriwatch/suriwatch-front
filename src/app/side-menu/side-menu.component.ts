import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FilteringContextService } from '../core/context/filtering-context.service';
import { Subscription } from 'rxjs';




@Component({
  selector: 'sw-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
 
  public searchParams = {}
  ctxtChangeSubscription: Subscription;


  constructor(
    private filterContext: FilteringContextService,
  ) { 
    
    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        this.searchParams = this.filterContext.getCtxtParams()
    });

  }

  ngOnInit(){
    
  }

  ngAfterViewInit(){
  }


}

