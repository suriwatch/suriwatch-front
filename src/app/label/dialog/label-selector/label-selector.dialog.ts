import { Component, OnInit, Inject } from '@angular/core';
import { Label } from '../../label';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LabelService } from '../../label.service';


export interface LabelSelectorDialogData {
  label : string
  value : string
}

@Component({
  selector: 'sw-label-selector-dialog',
  templateUrl: './label-selector.dialog.html',
  styleUrls: ['./label-selector.dialog.scss']
})
export class LabelSelectorDialog implements OnInit {

  public labels : Label[] = [];
  public label: Label = null;

  constructor(
    public dialogRef: MatDialogRef<LabelSelectorDialog>,
    private labelService: LabelService,
    @Inject(MAT_DIALOG_DATA) public data: LabelSelectorDialogData
  ) {
    this.initLabel()
  }

  ngOnInit(){
    this.labelService.getLabels().subscribe(labels => {
      this.labels  = labels;
    })
  }

  private getLabelByName(name: string): Label{
    var filtered = this.labels.filter(
      label => label.name === name
    );

    if(filtered.length > 0){
      return filtered[0];
    }
    return null;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  onLabelChange(event): void{
    this.label = this.getLabelByName(event.source.value);
    //this.data.label = this.label.name;
  }
/*
  onValueChange(event): void{
    this.data.value = event.source.value;
  }
*/
  initLabel(){
    this.label = { name: "",values: []};
  }
}