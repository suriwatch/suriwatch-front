import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { Observable } from 'rxjs';
import { Label } from './label';

@Injectable({
  providedIn: 'root'
})
export class LabelService {

  constructor(private api: APIService) {
  }

  getLabel(name: string): Observable<Label> {
    return this.api.getJson(`/labels/${name}`)
  }

  getLabels(): Observable<Label[]> {
    return this.api.getJson(`/labels`)
  }


}
