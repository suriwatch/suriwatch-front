export interface Label{

    name: string;
    values: string[];
}

export interface KVFilter{
  
    type: FilterType;
    key: string;
    value: string  
}

export enum FilterType {
    LABEL,
    FIELD,
}