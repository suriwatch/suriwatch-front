import { Component, OnInit, OnDestroy } from '@angular/core';
import { timer, Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ProbeResult, ProbeLastResultCriteria } from '../../model/probe-result';
import { ProbeResultService } from '../probe-result.service';
import { KVFilter, FilterType } from 'src/app/label/label';
import { MatDialog } from '@angular/material';
import { LabelSelectorDialog } from 'src/app/label/dialog/label-selector/label-selector.dialog';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';
import { query } from '@angular/animations';


@Component({
  selector: 'app-last-status',
  templateUrl: './last-status.component.html',
  styleUrls: ['./last-status.component.scss']
})
export class LastStatusComponent implements OnInit, OnDestroy {

  private timerSubscription: Subscription = null

  // Graph reload interval in milliseconds
  reloadInterval  = 30000;

  submitted = false;
  
  ctxtChangeSubscription: Subscription;
  
  results$ : Observable<ProbeResult[]> = null;

  constructor(
    private probeResultService: ProbeResultService,
    private router: Router,
    private filterContext: FilteringContextService,
  ) {
    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        this.search()
    });

  }

  ngOnInit() {
    this.results$  = this.probeResultService.getLastResults({});
    this.subscribeToData()
  }

  ngOnDestroy() {
    this.ctxtChangeSubscription.unsubscribe();
    this.timerSubscription.unsubscribe();
  }

  get filters() {
    return this.filterContext.getFilters();
  }

  selecteLabel(kvFilter: KVFilter): void{
    this.filterContext.addFilter(kvFilter); 
  }

  selectStatus(status: string): void{
    this.filterContext.addFilter(
    {
      "key": "status",
      "value": status,
      "type": FilterType.FIELD,
     });
  }

  selecteType(type: string): void{
    this.filterContext.addFilter(
      {
        "key": "type",
        "value": type,
        "type": FilterType.FIELD,
       });
  }

  onContextChange(){
    this.search()
  }

  search() { 
    this.submitted = true; 

    var criteria : ProbeLastResultCriteria  = {
      labels: this.filterContext.serialiseLabels(),
      query: this.filterContext.getQuery()
    }

    let statusFilter =  this.filterContext.getFieldFilter("status")
    if(statusFilter !== undefined){
      criteria.status = statusFilter.value
    }

    let typeFilter =  this.filterContext.getFieldFilter("type")
    if(typeFilter !== undefined){
      criteria.type  = statusFilter.value
    }

    this.results$ = this.probeResultService.getLastResults(criteria) 
  }


  onSelectResult(probeName: string){
    this.router.navigate(["probes", probeName, "detail"]);
  }

  private subscribeToData(): void {
    this.timerSubscription = timer(0, this.reloadInterval).subscribe(() => this.search())
  }


}

