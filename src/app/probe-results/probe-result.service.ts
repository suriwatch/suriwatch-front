import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { Observable } from 'rxjs';
import {ProbeResult, ProbeResultGroup, ProbeLastResultCriteria, ProbeResultHistogram, GroupStatusProbeCriteria} from '../model/probe-result'

@Injectable({
  providedIn: 'root'
})
export class ProbeResultService {

  constructor(private api: APIService) {}

  getLastResults(criteria: ProbeLastResultCriteria): Observable<ProbeResult[]> {

    let paramsList: string[] = [];
    let params = ""

    if(criteria.labels){
      paramsList.push("labels=" + criteria.labels)
    }
    if(criteria.status){
      paramsList.push("status=" + criteria.status);
    }
    if(criteria.host){
      paramsList.push("host="+criteria.host);
    }
    if(criteria.type){
      paramsList.push("type="+criteria.type);
    }
    if(criteria.query){
      paramsList.push("q="+criteria.query);
    }
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }

    return this.api.getJson(`/results/_last${params}`)
  }


  getProbeHisto(name: string, node?: string, from?: string, to?: string, interval?: string): Observable<ProbeResultHistogram> {
   
    let paramsList: string[] = [];
    let params = ""

    if(node){
      paramsList.push("node="+node);
    }
    if(from){
      paramsList.push("from="+from);
    }
    if(to){
      paramsList.push("to="+to);
    }
    if(interval){
      paramsList.push("interval="+interval);
    }
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }
    return this.api.getJson(`/probes/${name}/_histogram${params}`)
  }


  getGroupStatusProbe(criteria: GroupStatusProbeCriteria ): Observable<ProbeResultGroup> {

    let paramsList: string[] = [];
    let params = ""
    
    if(criteria.groups){
      paramsList.push("groups=" + criteria.groups.join(","))
    }
    if(criteria.labels){
      paramsList.push("labels=" + criteria.labels)
    }
    if(criteria.status){
      paramsList.push("status=" + criteria.status);
    }
    if(criteria.state){
      paramsList.push("state=" + criteria.state);
    }
    if(criteria.host){
      paramsList.push("host="+criteria.host);
    }
    if(criteria.type){
      paramsList.push("type="+criteria.type);
    }
    if(criteria.query){
      paramsList.push("q="+criteria.query);
    }
    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
      console.log(paramsList.join("&"))
    }
    

    return this.api.getJson(`/results/_search${params}`)
  }
  

}


