import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";


import { ViewProbesComponent} from "./probe/view-probes/view-probes.component";
import { EditProbeComponent } from "./probe/edit-probe/edit-probe.component";
import { LastStatusComponent } from "./probe-results/last-status/last-status.component";
import { DetailProbeComponent } from "./probe/detail-probe/detail-probe.component";
import { DashboardComponent } from "./dashboard/dashboard/dashboard.component";
import { HostDetailComponent } from "./dashboard/host-detail/host-detail.component";
import { ViewNodesStatusComponent } from "./node/view-nodes-status/view-nodes-status.component";
import { ViewStatusPagesComponent } from "./status-page/view-status-pages/view-status-pages.component";
import { DetailStatusPageComponent } from "./status-page/detail-status-page/detail-status-page.component";
import { EditStatusPageComponent } from "./status-page/edit-status-page/edit-status-page.component";


export const PROBES_ROUTING = [
    {path: "", redirectTo: '/probes',  pathMatch: 'full'},

    {path: "probes", component: ViewProbesComponent},
    {path: "probes/new", component: EditProbeComponent},
    {path: "probes/:id/edit", component: EditProbeComponent},
    {path: "probes/:id/copy", component: EditProbeComponent},
    {path: "probes/:id/detail", component: DetailProbeComponent},
    
    {path: "results/last", component: LastStatusComponent},

    {path: "status-pages", component: ViewStatusPagesComponent},
    {path: "status-pages/new", component: EditStatusPageComponent},
    {path: "status-pages/:id/detail", component: DetailStatusPageComponent},
    {path: "status-pages/:id/edit", component: EditStatusPageComponent},

    {path: "nodes", component: ViewNodesStatusComponent},

    {path: "dashboards", component: DashboardComponent},
    {path: "hosts/:name/detail", component: HostDetailComponent},
]

const routes: Routes = [
    ...PROBES_ROUTING

];

@NgModule({
  imports: [RouterModule.forRoot(
    routes
    /*
     , {enableTracing: true}
     */
  )],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
