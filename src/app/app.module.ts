import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { DetailProbeComponent } from './probe/detail-probe/detail-probe.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule, BarHorizontalStackedComponent } from '@swimlane/ngx-charts'
import { FormsModule } from '@angular/forms';
import { ProbeService } from './probe/probe.service';
import { APIService } from './core/api/api.service';
import { LabelService } from './label/label.service';
import { HttpModule } from '@angular/http';
import { SideMenuComponent } from './side-menu/side-menu.component';


import { ViewProbesComponent } from './probe/view-probes/view-probes.component';
//import { NgxUIModule } from '@swimlane/ngx-ui';
import {AppRoutingModule} from "./app-routing.module";
import { EditProbeComponent } from './probe/edit-probe/edit-probe.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsComponent } from './core/show-errors.component';
import { LastStatusComponent } from './probe-results/last-status/last-status.component';
import { StatusIconComponent } from './core/components/status-icon/status-icon.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { CardComponent } from './dashboard/card/card.component';
import { HostDetailComponent } from './dashboard/host-detail/host-detail.component';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatFormFieldModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  MAT_DATE_LOCALE,
} from '@angular/material';
import { LabelSelectorDialog } from './label/dialog/label-selector/label-selector.dialog';
import { RemoveConfirmDialog } from './core/dialog/remove-confirm/remove-confirm.dialog';
import { PeriodSelectorDialog } from './core/dialog/period-selector/period-selector.dialog';
import { NodeService } from './node/node.service';
import { ViewNodesStatusComponent } from './node/view-nodes-status/view-nodes-status.component';
import { LabelsDisplayComponent } from './core/components/labels-display/labels-display.component';
import { ResponseTimeChartComponent } from './probe/detail-probe/response-time-chart/response-time-chart.component';
import { StatusChartComponent } from './probe/detail-probe/status-chart/status-chart.component';
import { FilteringContextService } from './core/context/filtering-context.service';
import { FilteringContextComponent } from './context/filtering-context/filtering-context.component';
import { ProbeTypeDisplayComponent } from './core/components/probe-type-display/probe-type-display.component';
import { ProbeModeDisplayComponent } from './core/components/probe-mode-display/probe-mode-display.component';
import { ProbeStateDisplayComponent } from './core/components/probe-state-display/probe-state-display.component';
import { EventsViewComponent } from './probe/detail-probe/events-view/events-view.component';
import { DatetimePickerComponent } from './core/components/datetime-picker/datetime-picker.component';
import { PeriodPickerComponent } from './core/components/period-picker/period-picker.component';
import { ViewStatusPagesComponent } from './status-page/view-status-pages/view-status-pages.component';
import { DetailStatusPageComponent } from './status-page/detail-status-page/detail-status-page.component';
import { DurationDisplayComponent } from './core/components/duration-display/duration-display.component';
import { EventStatusDisplayComponent } from './core/components/event-status-display/event-status-display.component';
import { EditStatusPageComponent } from './status-page/edit-status-page/edit-status-page.component';



export const SIDE_MENU_COMPONENTS = [
  SideMenuComponent
]

export const PROBE_STATUS_COMPONENTS = [
  LastStatusComponent
]
export const PROBE_COMPONENTS = [
  ViewProbesComponent,
  EditProbeComponent,
  DetailProbeComponent,
]

export const DIALOG_COMPONENTS = [
  LabelSelectorDialog,
  RemoveConfirmDialog,
  PeriodSelectorDialog,
]

export const MAT_COMPONENTS = [
  MatSidenavModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule, 
  MatButtonModule,
  MatSelectModule,
  MatDialogModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatRadioModule,
  MatChipsModule,
  MatTooltipModule,
  MatProgressSpinnerModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatAutocompleteModule,
]

@NgModule({
  declarations: [
    AppComponent,
    ...SIDE_MENU_COMPONENTS,
    ...PROBE_STATUS_COMPONENTS,
    ...PROBE_COMPONENTS,
    ShowErrorsComponent,
    StatusIconComponent,
    DashboardComponent,
    CardComponent,
    HostDetailComponent,
    ...DIALOG_COMPONENTS,
    ViewNodesStatusComponent,
    LabelsDisplayComponent,
    ResponseTimeChartComponent,
    StatusChartComponent,
    FilteringContextComponent,
    ProbeTypeDisplayComponent,
    ProbeModeDisplayComponent,
    ProbeStateDisplayComponent,
    EventsViewComponent,
    DatetimePickerComponent,
    PeriodPickerComponent,

    DurationDisplayComponent,
    EventStatusDisplayComponent,

    ViewStatusPagesComponent,
    DetailStatusPageComponent,
    EditStatusPageComponent,

  ],
  entryComponents: [ 
    ...DIALOG_COMPONENTS,
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    //NgxUIModule,
    ...MAT_COMPONENTS,
  ],
  providers: [
    APIService,
    ProbeService,
    LabelService,
    NodeService,
    FilteringContextService,
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


