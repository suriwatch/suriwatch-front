import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { Observable } from 'rxjs';
import { StatusChangeEvent, EventQuery } from '../model/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private api: APIService) {
  }
  
  public search(query: EventQuery): Observable<StatusChangeEvent[]>{

    let paramsList: string[] = [];
    let params = ""

    if(query.probeId){
      paramsList.push("probeId=" + query.probeId)
    }
    if(query.labels){
      paramsList.push("labels=" + query.labels)
    }
    if(query.from){
      paramsList.push("from=" + query.from)
    }
    if(query.to){
      paramsList.push("to=" + query.to)
    }
    if(query.sort){
      let sortValues = []
      for (let sortValue of query.sort) {
        sortValues.push(sortValue.name +":"+sortValue.order)
      }
      paramsList.push("sort=" + sortValues.join(":"))
    }

    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }
    return this.api.getJson(`/events${params}`)
  }

}
