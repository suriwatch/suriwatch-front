import { Component, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { FilteringContextService } from 'src/app/core/context/filtering-context.service';
import { LabelSelectorDialog } from 'src/app/label/dialog/label-selector/label-selector.dialog';
import { MatDialog } from '@angular/material';
import { KVFilter, FilterType } from 'src/app/label/label';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { getMatScrollStrategyAlreadyAttachedError } from '@angular/cdk/overlay/typings/scroll/scroll-strategy';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';



@Component({
  selector: 'sw-filtering-context',
  templateUrl: './filtering-context.component.html',
  styleUrls: ['./filtering-context.component.scss']
})
export class FilteringContextComponent implements OnInit {

  @Output() change: EventEmitter<any> = new EventEmitter();
  
  public query: string;
   
  public ctxtChangeSubscription: Subscription;

  constructor(
    private filterContext: FilteringContextService,
    public dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location

  ) { 
  
    // Subscribe on context change
    this.ctxtChangeSubscription = filterContext.ctxtChange$.subscribe(
      change => {
        if( change.emitter !== FilteringContextComponent.name){
          this.updateUrl()
        }
    });
  }



  ngOnInit() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(e => { this.initCtxt()});
  }

  private deserializeLabels(serializedLabels: string) :KVFilter[] {

    let res :KVFilter[]  = []
    if(serializedLabels){
      let labelsTab: string[] = serializedLabels.split(',')

      labelsTab.forEach(serializedLabels => {
  
        if(serializedLabels.includes(":")){
          var labelTab: string[] = serializedLabels.split(":")
  
          if(labelTab.length == 2){
            res.push({
              "key": labelTab[0],
              "value": labelTab[1],
              "type": FilterType.LABEL,
            });       
          }
        }
      });
    }
    return res;
}


initCtxt(){
  // Initialize context with url parameterss
  this.activatedRoute.queryParams.subscribe(params => {

    // If context is given, use it.
    if( this.isContextGivent(params)){
      this.filterContext.setContext(
        params['grp'],
        params['q'],
        this.deserializeLabels(params['labels']),
        FilteringContextComponent.name
      )
    }
  });
}

isContextGivent(params) :boolean{
  let contextKeys = ['grp', 'q', 'labels']
  for(let key of contextKeys){
    if( key in params){
      return true;
    }
  }
  return false;
}


  get filters() {
    return this.filterContext.getFilters();
  }

  openAddFilterDialog(): void {
         
    const dialogRef = this.dialog.open(LabelSelectorDialog, {
      width: '500px',
      data: { name:"", value:""}
    });

    dialogRef.afterClosed().subscribe(result => {
      let filter: KVFilter  = {"key": result.label, "value": result.value, "type": FilterType.LABEL,}
      this.filterContext.addFilter(filter, FilteringContextComponent.name)
      
      this.change.emit(null);

      this.updateUrl()
    });
  }


  updateUrl(){
  
    let strLabels = this.filterContext.serialiseLabels()
    let strGroup = this.filterContext.getGroup()
    let strQuery = this.filterContext.getQuery()

      this.router.navigate([], {
         relativeTo: this.activatedRoute,
         queryParams: {
           labels:  strLabels,
            grp: strGroup,
            q:strQuery,     
         },
         queryParamsHandling: 'merge',
         // preserve the existing query params in the route
         skipLocationChange: false, //true
         // do not trigger navigation
      }); 
  }

  updateQuery(){
    this.filterContext.setQuery(this.query)
    this.change.emit(null);
  }

  removeFilter(removedfilter){
    this.filterContext.removeFilter(removedfilter)
    this.change.emit(null);
  }


  

}
