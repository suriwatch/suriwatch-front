import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusPageService } from '../status-page.service';
import { StatusPageStatus, ProbeAvailability } from 'src/app/model/status-page';
import { ProbeResult } from 'src/app/model/probe-result';

@Component({
  selector: 'sw-detail-status-page',
  templateUrl: './detail-status-page.component.html',
  styleUrls: ['./detail-status-page.component.scss']
})
export class DetailStatusPageComponent implements OnInit {

  public loading: boolean
  public pageId: string

  private mapProbeResultById : Map<string, ProbeResult> = new Map() 
  private mapProbeAvailabilityById : Map<string, ProbeAvailability> = new Map() 


  public statusPageStatus: StatusPageStatus = null

  constructor(
    private route: ActivatedRoute,
    private statusPageService: StatusPageService,
  ) { 
    this.route.params.subscribe(params => {
      this.pageId = params["id"];
    });  
  }

  ngOnInit() {
    this.search()
  }

  search(){
    this.loading = true
    this.statusPageService.getStatusPageStatus(this.pageId).subscribe(
        statusPageStatus => { 
          
            this.statusPageStatus = statusPageStatus
        
            statusPageStatus.availabilities.forEach(availability => {
              this.mapProbeAvailabilityById.set(availability.probeId, availability)
            });

            statusPageStatus.results.forEach(result => {
              this.mapProbeResultById.set(result.probeId, result)
            });


            this.loading = false
        }, 
        error => { this.loading = false} 
    )
  }



  public getProbeResult(probeId: string): ProbeResult{
    return this.mapProbeResultById.get(probeId)
  }
  public getProbeAvailability(probeId: string): ProbeAvailability{
    return this.mapProbeAvailabilityById.get(probeId)
  }

}
