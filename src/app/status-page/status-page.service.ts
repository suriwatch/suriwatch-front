import { Injectable } from '@angular/core';
import { APIService } from '../core/api/api.service';
import { StatusPage, SearchStatusPageQuery, StatusPageStatus } from '../model/status-page';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatusPageService {

  constructor(private api: APIService) {
  }

  getStatusPage(id: string): Observable<StatusPage> {
    return this.api.getJson(`/status-pages/${id}`)
  }

  searchStatusPage(query: SearchStatusPageQuery): Observable<StatusPage[]>{

    let paramsList: string[] = [];
    let params = ""
   
    if(query.size !== undefined){
      paramsList.push(`size=${query.size}`)
    }else{
      paramsList.push(`size=250`)
    }

    if(paramsList.length > 0){
      params = "?" + paramsList.join("&")
    }

    return this.api.getJson(`/status-pages${params}`)
  }

  save(statusPage: StatusPage): Observable<StatusPage>{
    return this.api.postJson(`/status-pages`, statusPage);
  }

  delete(id: string): Observable<void>{
    return this.api.delete(`/status-pages/${id}`)
  }

  getStatusPageStatus(id: string): Observable<StatusPageStatus>{
    return this.api.getJson(`/status-pages/${id}/status`);
  }

}
