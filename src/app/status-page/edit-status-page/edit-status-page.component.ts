import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { StatusPageService } from '../status-page.service';
import { StatusPage } from 'src/app/model/status-page';

@Component({
  selector: 'sw-edit-status-page',
  templateUrl: './edit-status-page.component.html',
  styleUrls: ['./edit-status-page.component.scss']
})
export class EditStatusPageComponent implements OnInit {
  
  statusPageForm: FormGroup;
  
  statusPage$ : Observable<StatusPage> = null;
  statusPageId: string = null;
  form_submited: boolean = false


  constructor(
    private statusPageService: StatusPageService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router) { 

    this.route.params.subscribe(params => {
      this.statusPageId = params["id"];
      this.statusPageForm = this.getInitializedPageForm()

      const statusPage = {} as StatusPage;

      if( this.statusPageId){
        this.loadStatusPage(this.statusPageId);
      }else{
        this.statusPage$ = of(statusPage);
      }

    });  
  }

  ngOnInit() {
  
  }

  /**
   * Initialiaze probe formulaire and attach form listeners
   */
  getInitializedPageForm(){

    let formGroup =  this.formBuilder.group({
      name: new FormControl(null, Validators.required ),
      description: new FormControl(null ),
      labels: new FormArray([new FormGroup({
        name: new FormControl(null, Validators.required),
        value: new FormControl(null, Validators.required)
      })], Validators.required),
    });

    return formGroup;
  }


  /**
   * Remove all validator for the give group
   * @param group 
   */
  removeFormGroupValidators(group: FormGroup){
    Object.keys(group.controls).forEach(key => {
        group.controls[key].setValidators(null);
        group.controls[key].updateValueAndValidity();
    });
  }



  /**
   * Retrieve the probe form the API and load the probe form 
   *
   * @param probeId 
   * 
   */
  loadStatusPage(statusPageId: string){

    // Getting http probe
    this.statusPage$ = this.statusPageService.getStatusPage(statusPageId);

    this.statusPage$.subscribe(statusPage => {
      this.modelToForm(statusPage)
    })
  }




  buildEntry(): FormGroup {
    return new FormGroup({
      name: new FormControl('',Validators.required),
      value: new FormControl()})
  }


  initFormGroupHeader(header ): FormGroup{
    let fg = this.buildEntry();
    fg.patchValue(header)
    return fg
  }



  modelToForm(statusPage: StatusPage){


    // Create a patch object and apply it (/!\ Can't use Object.assign form deep copy, )
    var formPatch = JSON.parse(JSON.stringify(statusPage));

    // Don't want to apply patchValue on labels 
    delete formPatch["labels"]; 

    this.statusPageForm.patchValue(formPatch)

    // Initialize probe labels form   
    let labelControlArray = <FormArray>this.statusPageForm.get('labels')   

    if(labelControlArray.length > 0){
      labelControlArray.removeAt(0)
    }

    if(statusPage.labels){
      for (let key in statusPage.labels) {
        labelControlArray.push(  new FormGroup({
          name: new FormControl(key,Validators.required),
          value: new FormControl( statusPage.labels[key])
        }))
      }
    }

  }

  /**
   * 
   */
  formToModel(): StatusPage{
    const result: StatusPage = Object.assign({}, this.statusPageForm.value);
    result.id = this.statusPageId 

    if( result.labels){
      var labelsObj = {}
      result.labels.forEach(label => {
        labelsObj[ (<any> label).name ] = label.value
      });
      result.labels = labelsObj
    }

    return result
  }



  submit() {
    this.form_submited = true 

    if( this.statusPageForm.valid ) {
      let statusPageToSave = this.formToModel()


      this.statusPageService.save(statusPageToSave).subscribe(page => {
        this.router.navigate(["status-pages"]);
      });
    }
  }


}
