import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StatusPage, SearchStatusPageQuery } from 'src/app/model/status-page';
import { StatusPageService } from '../status-page.service';
import { RemoveConfirmDialog } from 'src/app/core/dialog/remove-confirm/remove-confirm.dialog';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'sw-view-status-pages',
  templateUrl: './view-status-pages.component.html',
  styleUrls: ['./view-status-pages.component.scss']
})
export class ViewStatusPagesComponent implements OnInit {
 
  pages$ : Observable<StatusPage[]> = null;


  constructor(
    public dialog: MatDialog,
    private statusPageService: StatusPageService,
  ) { }

  ngOnInit() {
    this.search();
  }


  search() { 


    let query: SearchStatusPageQuery = {}
    
    this.pages$ = this.statusPageService.searchStatusPage(query) 

  }

  onDeleteStatusPage(page: StatusPage){
    
    const dialogRef = this.dialog.open(RemoveConfirmDialog, {
      width: '500px',
      data: { entity: page.name}
    });

    dialogRef.afterClosed().subscribe(result => {
     
      if(result){
        this.statusPageService.delete(page.id).subscribe(_ => {
          this.search()
        });
      }
    });
  }

}
