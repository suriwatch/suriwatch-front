FROM alpine

EXPOSE 8080

# add the user and create its working dir
RUN apk add --no-cache nginx bash && \
    adduser -D -u 1001 1001 && \
    mkdir -p /dist && chown -R 1001:1001 /dist

COPY docker/nginx.conf /etc/nginx/nginx.conf
#COPY docker/entrypoint.sh /entrypoint.sh
#RUN rm -rf /etc/nginx/conf.d 
#RUN chown 1001:1001 /entrypoint.sh && chmod +x /entrypoint.sh

COPY dist/suriwatch-front/. /dist
RUN chown -R 1001:1001 /dist

USER 1001
WORKDIR /dist

# RUN
#ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx"]


